package com.saksit.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var oilArray: ArrayList<Oil>;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val oilData = arrayOf(
            arrayOf("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ E20:",34.44),
            arrayOf("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 91:",35.28),
            arrayOf("เชลล์ ฟิวเซฟ แก๊สโซฮอล์ 95:",35.55),
            arrayOf("เชลล์ วี-เพาเวอร์ แก๊สโซฮอล์ 95:",43.04),
            arrayOf("เชลล์ ดีเซล B20:",34.94),
            arrayOf("เชลล์ ฟิวเซฟ ดีเซล:",34.94),
            arrayOf("เชลล์ ฟิวเซฟ ดีเซล B7:",34.94),
            arrayOf("เชลล์ วี-เพาเวอร์ ดีเซล:",34.94),
            arrayOf("เชลล์ วี-เพาเวอร์ ดีเซล B7:",45.66)
        )

        oilArray = ArrayList()

        for(oil in oilData){
            val oilInit = Oil(oil[0].toString(), oil[1].toString().toDouble())
            oilArray.add(oilInit)
        }
        var recyclerView: RecyclerView? = findViewById(R.id.oil_price_recycler);
        recyclerView?.layoutManager = LinearLayoutManager(this)
        var adapter = Adapter(oilArray);
        recyclerView?.adapter = adapter;

        adapter.setOnItemClickListener(object : Adapter.onItemClickListener {
            override fun onItemClick(position: Int) {
                var description = "${oilArray[position].getProductName().toString()} / ${oilArray[position].getProductPrice().toString()}"
                Toast.makeText(applicationContext, description, Toast.LENGTH_SHORT).show()
                Log.d("OnOilClick", description)
            }
        })


    }
}