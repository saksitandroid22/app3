package com.saksit.app3

class Oil(name: String, price: Double) {
    private val name = name
    private val price = price

    fun getProductName():String{
        return this.name
    }
    fun getProductPrice():Double{
        return this.price
    }
}